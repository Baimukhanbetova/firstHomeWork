﻿/*Создайте приложение, которое хранит цветовую схему для каждого пользователя и автоматически 
 * загружает корректную цветовую схему при его открытии пользователем*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FirstHomeWork
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<User> users;
        public MainWindow()
        {
            InitializeComponent();
            users = new List<User>
            {
               new User()  { Login = "user1", Password = "123" , Color=System.Drawing.Color.Black },
               new User() { Login = "user2", Password = "234", Color=System.Drawing.Color.Blue}
            };
        }

        private void SignInButton_Click(object sender, RoutedEventArgs e)
        {
             for (int i = 0; i < users.Count; i++)
            {
                string login = users[i].Login;
                string password = users[i].Password;
                System.Drawing.Color color = users[i].Color;
                if (loginTextBox.Text.Equals(login) && passwordTextBox.Text.Equals(password))
                {
                    MainMenu menu = new MainMenu();
                    menu.BackColor = color;
                    menu.Show();
                    
                }
                else
                {
                    MessageBox.Show("Неверный логин или пароль");                    
                }
            }
        }
    }
}
